// js engine may not be compiled with crypto, so check
let crypto;
try {
  crypto = require('crypto');
} catch (err) {
  console.log('crypto support is disabled : ' + err);
}

let secureRandom;
let seedRandom;
try {
  secureRandom = require('secure-random');
  seedRandom = require('seedrandom');
} catch (err) {
  console.log('a dependency is missing : ' + err);
}

// https://gist.github.com/jabney/5018b4adc9b2bf488696
// Shannon entropy in bits per symbol.
function entropy(str) {
  const len = str.length

  // Build a frequency map from the string.
  const frequencies = Array.from(str)
    .reduce((freq, c) => (freq[c] = (freq[c] || 0) + 1) && freq, {})

  // Sum the frequency of each character.
  return Object.values(frequencies)
    .reduce((sum, f) => sum - f/len * Math.log2(f/len), 0)
}

const generator = (name, result, metric) => {
  return {
    logName: () => { console.log(name) },
    logResult: () => { console.log(result) },
    logMetric: () => { console.log(metric) },
    log: () => {
      console.log(name + ' : ' + result + ' | ' + metric(result) + '/chr, ' + metric(result)*result.length/4 +  '/str \n');
    }
  }
}

console.log('\n\n##########################################################\n')
console.log('Random Number Source : Results | Shannon Entropy (bits)');
console.log('\n##########################################################\n')

const mathDotRandom = generator('Math.random()', Math.random().toString().substring(2), entropy);
mathDotRandom.log();

const cryptoRandomInt = generator('crypto.RandomInt()', crypto.randomInt(10e10).toString(), entropy);
cryptoRandomInt.log();

const cryptoRandomBytes = generator('crypto.RandomBytes()', crypto.randomBytes(256).toString('hex'), entropy);
cryptoRandomBytes.log();

const secureRandomObj = generator('secureRandom()', secureRandom(8).join('').toString(), entropy);
secureRandomObj.log();

let seedboring = seedRandom('hello');
const seedBoringObj = generator("seedRandom('hello')", seedboring().toString().substring(2), entropy);
seedBoringObj.log();

const cryptoRB256 = crypto.randomBytes(256).toString('hex')
const seed256 = seedRandom(cryptoRB256);

const seed256Obj = generator("seedRandom(crypto-256-byte-string)", seed256().toString().substring(2), entropy);
seed256Obj.log();

let megaSeed256 = '';
let megaResult = '';
for (i = 0; i < 8; i++) {
	let hex256 = '';
	hex256 += crypto.randomBytes(256).toString('hex');
	megaSeed256 = seedRandom(hex256);
	megaResult += megaSeed256().toString().substring(2);
}

const megaSeedObj = generator('seedRandom(crypto.randomBytes(256)', megaResult, entropy);
megaSeedObj.log();


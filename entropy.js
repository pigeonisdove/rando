// entropy.js MIT License © 2014 James Abney http://github.com/jabney
// ES6 portation MIT License © 2017 Peter Seprus http://github.com/ppseprus
// Adds more formulas MIT License © 2020 Pigeonisdove https://github.com/pigeonisdove

// based on https://gist.github.com/ppseprus/afab8500dec6394c401734cb6922d220#file-entropy-js

// Calculate the Shannon entropy of a string in bits per symbol.
(function (shannon) {
    'use strict';

    // log2 polyfill for ie
    if (!Math.log2) Math.log2 = function(x) {
      return Math.log(x) * Math.LOG2E;
    };

    // Create an array of character frequencies.
    const getFrequencies = str => {
        let dict = new Set(str);
        return [...dict].map(chr => {
            return str.match(new RegExp(chr, 'g')).length;
        });
    };

    // Measure the entropy of a string in bits per symbol.
    shannon.entropy = str => getFrequencies(str)
        .reduce((sum, frequency) => {
            let p = frequency / str.length;
            return sum - (p * Math.log2(p));
        }, 0);

    // Measure the maximum entropy of a string in bits per symbol.
    shannon.maxentropy = str => {
      if (str.length <= 1) { return 0 } else {
        return Math.log2(str.length);
      }
    };

    // Measure the normalized entropy of a string in bits per symbol.
    shannon.normentropy = str => {
      if (str.length <= 1) { return 0 } else {
        return shannon.entropy(str) / shannon.maxentropy(str);
      }
    };

    // Measure the metric entropy of a string.
    shannon.metricentropy = str => {
      if (str.length <= 1) { return 0 } else {
        return shannon.entropy(str) / str.length;
      }
    };

    // Measure the entropy of a string in total bits.
    shannon.bits = str => shannon.entropy(str) * str.length;

    // Log the entropy of a string to the console.
    shannon.log = str => console.log(
      `Entropy of "${str}" in bits per symbol:`,
      shannon.entropy(str),
      `\nMaximum Entropy of "${str}":`,
      shannon.maxentropy(str),
      `\nNormalized Entropy of "${str}":`,
      shannon.normentropy(str),
      `\nMetric Entropy of "${str}":`,
      shannon.metricentropy(str)
    );

})(window.shannon = window.shannon || Object.create(null));

shannon.log('1223334444');          // 1.8464393446710154
shannon.log('');                    // 0
shannon.log('0');                   // 0
shannon.log('01');                  // 1
shannon.log('0123');                // 2
shannon.log('01234567');            // 3
shannon.log('0123456789abcdef');    // 4
shannon.log('15d35d84a737d0af');    // 3.202819531114783
